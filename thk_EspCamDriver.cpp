#include "Arduino.h"
#include "thk_EspCamDriver.h"

thk_EspCamDriver::thk_EspCamDriver(espboard_t board, esp_image_t *img)
    : image(img), board(board)
{
  image->width = board.im_width;
  image->height = board.im_height;
  pinMode(board.cpin1, INPUT_PULLUP);
  pinMode(board.cpin2, INPUT_PULLUP);
  config = new camera_config_t;
  error = new esp_err_t;
};

thk_EspCamDriver::thk_EspCamDriver(espboard_t board, framesize_t s, pixformat_t f, esp_image_t *img)
    : thk_EspCamDriver(board, img)
{
  set_framesize(s);
  set_pixelformat(f);
};

// Erzeugt eine Konfiguration basierend auf Klassenattributen
// Muss vor dem Auslesen der Kamera erfolgen!
void thk_EspCamDriver::create_config()
{
  config->ledc_channel = board.ledc_channel;
  config->ledc_timer = LEDC_TIMER_0;
  config->pin_d0 = board.pin_d0;
  config->pin_d1 = board.pin_d1;
  config->pin_d2 = board.pin_d2;
  config->pin_d3 = board.pin_d3;
  config->pin_d4 = board.pin_d4;
  config->pin_d5 = board.pin_d5;
  config->pin_d6 = board.pin_d6;
  config->pin_d7 = board.pin_d7;

  config->pin_xclk = board.pin_xclk;
  config->pin_pclk = board.pin_pclk;
  config->pin_vsync = board.pin_vsync;
  config->pin_href = board.pin_href;
  config->pin_sscb_sda = board.pin_sscb_sda;
  config->pin_sscb_scl = board.pin_sscb_scl;
  config->pin_pwdn = board.pin_pwdn;
  config->pin_reset = board.pin_reset;
  config->xclk_freq_hz = board.xclk_freq_hz;
  config->frame_size = board.frame_size;

  config->pixel_format = board.pixel_format; // for streaming
  config->jpeg_quality = board.jpeg_quality;
  config->fb_count = board.fb_count;

  Serial.println("[OK] Camera config created");
}

// Initialisiert die ESP Kamera mit der Konfiguration
void thk_EspCamDriver::init_camera()
{
  *error = esp_camera_init(config);
  if (*error != ESP_OK)
  {
    Serial.printf("[!] Camera failed 0x%x \n", *error);
    return;
  }
  else
  {
    Serial.println("[OK] Camera init successfully");
    camerasensor = esp_camera_sensor_get();
  }
}

// Erzeugt ein Bild mithilfe des Kameramoduls vom Typ Zeiger auf camera_fb_t
bool thk_EspCamDriver::take_picture(esp_image_t *img)
{
  img->framebuffer = esp_camera_fb_get();
  if (!img->framebuffer)
  {
    return false;
  }
  else
  {
    image->buf_len = image->framebuffer->len;
    image->buf = image->framebuffer->buf;
  }
  return true;
}

// Beendet Datenübertragung und leert allokierten Speicher
// Muss immer vor dem nächsten take_picture() erfolgen!
void thk_EspCamDriver::free_buffer(esp_image_t *img)
{
  esp_camera_fb_return(img->framebuffer);
  img->framebuffer = NULL;
  img->buf = NULL;
}
// Initialisiert das Kameramodul (erstellt Konfiguration, startet Kamera)
void thk_EspCamDriver::init()
{
  Serial.println("# Starte Treiber");
  this->create_config();
  this->init_camera();
  camerasensor->set_vflip(camerasensor, vflip);     // flip it back
  camerasensor->set_hmirror(camerasensor, hmirror); // flip it back
  if (grscl)
    camerasensor->set_special_effect(camerasensor, 2); // 0 to 6 (0 - No Effect, 1 - Negative, 2 - Grayscale, 3 - Red Tint, 4 - Green Tint, 5 - Blue Tint, 6 - Sepia)
  this->take_picture(this->image);
  this->free_buffer(this->image);
}

// Hier könnten später Funktionen zur Bildmanipulation folgen ....
void thk_EspCamDriver::set_vflip(bool f)
{
  this->vflip = f;
}

void thk_EspCamDriver::set_hmirror(bool f)
{
  this->hmirror = f;
}
void thk_EspCamDriver::set_grayscale(bool f)
{
  this->grscl = f;
}

void thk_EspCamDriver::setCameraPins(int8_t camera_pin1, int8_t camera_pin2)
{
  board.cpin1 = camera_pin1;
  board.cpin2 = camera_pin2;
}
int8_t thk_EspCamDriver::getcpin1()
{
  return board.cpin1;
}
int8_t thk_EspCamDriver::getcpin2()
{
  return board.cpin2;
}
void thk_EspCamDriver::set_framesize(framesize_t size)
{
  board.frame_size = size;
  switch (size)
  {
  case (FRAMESIZE_96X96):
    image->height = 96;
    image->width = 96;
    break;
  case (FRAMESIZE_240X240):
    image->height = 240;
    image->width = 240;
    break;
  }
};
void thk_EspCamDriver::set_pixelformat(pixformat_t format)
{
  board.pixel_format = format;
};

// Skaliert ein Eingangsbild image vom Typ Zeiger auf esp_image_t um factor und schreibt das neue Bild img_out vom Typ Zeiger auf esp_image_t.
// Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
void thk_EspCamDriver::rescale_img(esp_image_t *image, esp_image_t *img_out, byte factor, bool serialout)
{
  int p = 0;
  int nwidth = image->width / factor;
  int nheight = image->height / factor;
  esp_image_t temp;
  temp.buf = new uint8_t[nwidth * nwidth];

  for (int y = 0; y < image->width; y += factor)
  {
    for (int x = 0; x < image->height; x += factor)
    {
      temp.buf[p] = image->buf[(y)*image->width + (x)];
      // Serial.print(p);Serial.print(" ");Serial.print((nwidth+y) * image.width + (x*factor));Serial.println("");
      p++;

      if (serialout)
      {
        int pixel = temp.buf[p];
        if (pixel > 224)
        {
          Serial.print(" ");
        }
        else if (pixel > 192)
        {
          Serial.print(" ");
        }
        else if (pixel > 160)
        {
          Serial.print(".");
        }
        else if (pixel > 128)
        {
          Serial.print("-");
        }
        else if (pixel > 96)
        {
          Serial.print("=");
        }
        else if (pixel > 64)
        {
          Serial.print("H");
        }
        else if (pixel > 32)
        {
          Serial.print("#");
        }
        else
        {
          Serial.print('M');
        };
        Serial.print(" ");
      }
    }
    if (serialout)
      Serial.println("");
  }
  img_out->width = nwidth;
  img_out->height = nheight;
  img_out->buf = temp.buf;
  img_out->buf_len = img_out->buf_len / factor;
  delete temp.buf;
};

// Konvertiere Bildbuffer von uint8_t zu int8_t
// Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
int8_t *thk_EspCamDriver::convertbuffer_INT8(uint8_t *buf_uint8, int16_t width, int16_t height, bool serialout)
{
  int8_t *buf_int8 = new int8_t[width * height];
  for (int y = 0; y < width; y++)
  {
    for (int x = 0; x < height; x++)
    {
      buf_int8[y * width + x] = ((int8_t *)buf_uint8)[y * width + x] - 128;
      if (serialout)
      {
        Serial.print(buf_int8[y * width + x]);
        Serial.print(" ");
      }
    }
    if (serialout)
      Serial.println("");
  }
  return buf_int8;
};

// Konvertiere Bildbuffer von uint8_t zu int8_t
// Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
float *thk_EspCamDriver::convertbuffer_FLOAT32(uint8_t *buf_uint8, int16_t width, int16_t height, bool serialout)
{
  int8_t *buf_int8 = new int8_t[width * height];
  float *buf_float = new float[width * height];

  for (int y = 0; y < width; y++)
  {
    for (int x = 0; x < height; x++)
    {
      buf_int8[y * width + x] = ((int8_t *)buf_uint8)[y * width + x] - 128;
      buf_float[y * width + x] = buf_int8[y * width + x];
      if (serialout)
      {
        Serial.print(buf_float[y * width + x]);
        Serial.print("\t");
      }
    }
    if (serialout)
      Serial.println("");
  }
  delete buf_int8;
  return buf_float;
};

// Binarisiere Bildbuffer der Länge len. Hierbei wird jeweils ein Pixel min oder max (>Schwellwert) gesetzt.
// In Zukunft besser als Template implementieren!
void thk_EspCamDriver::binarize_FLOAT32(float *buf_float, int32_t len, int16_t threshold, float max, float min, bool serialout)
{
  for (int pixel = 0; pixel < len; pixel++)
  {
    buf_float[pixel] >= threshold ? buf_float[pixel] = max : buf_float[pixel] = min;
    if (serialout)
    {
      Serial.print(buf_float[pixel], 2);
      Serial.print(" ");
      if (((pixel + 1) % 100) == 0)
        Serial.println(" ");
    }
  }
};