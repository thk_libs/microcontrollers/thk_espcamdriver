## Beschreibung
Treiber mit Beispielen für die Verwendung eines ESP32 Boards mit Kameramodul

## Installation
1. git Repository herunterladen


        git clone https://git-ce.rwth-aachen.de/thk_libs/microcontrollers/thk_espcamdriver.git


    oder als Zip

    ![](git_img.png)

2. Bibliothek ins lokale Arduino libraries Verzeichnis verschieben. \
Unter Windows: `Dokumente/Arduino/libraries/` \
(Falls als .zip heruntergeladen Bibliothek vorher entpacken)

## Anwendung
In der Arduino-IDE können die Beispiele unter Datei>Beispiele>espcamdriver geöffnet werden.

* cameraimage.ino \
    Kamerabild auslesen und auf dem seriellen Monitor ausgeben.

* cameraserver.ino \
    Streamen des Kamerabildes per lokalem Netzwerk.

* UnitTest.ino \
    Testfälle

### Erläuterung

Für die Verwendung eines Kameramoduls am ESP32 ist die Pinbelegung notwendig.
Diese sind in der seperaten Datei `camera_boards.h` hinterlegt. Gerne um neue Board erweitern!

    espboard_t waveshare_esp32_one{
        waveshare_esp32_one.cpin1 = 13,
        waveshare_esp32_one.cpin2 = 14,
        waveshare_esp32_one.pin_d0 = 34,
        waveshare_esp32_one.pin_d1 = 13,
        waveshare_esp32_one.pin_d2 = 14,
        waveshare_esp32_one.pin_d3 = 35,
        waveshare_esp32_one.pin_d4 = 39,
        waveshare_esp32_one.pin_d5 = 38,
        waveshare_esp32_one.pin_d6 = 37,
        waveshare_esp32_one.pin_d7 = 36,
        waveshare_esp32_one.pin_xclk = 4,
        waveshare_esp32_one.pin_pclk = 25,
        waveshare_esp32_one.pin_vsync = 5,
        waveshare_esp32_one.pin_href = 27,
        waveshare_esp32_one.pin_sscb_sda = 18,
        waveshare_esp32_one.pin_sscb_scl = 23,
        waveshare_esp32_one.pin_pwdn = -1,
        waveshare_esp32_one.pin_reset = -1,
        waveshare_esp32_one.xclk_freq_hz = 20000000,
        waveshare_esp32_one.jpeg_quality = 12,
        waveshare_esp32_one.fb_count = 1,
        waveshare_esp32_one.ledc_channel = LEDC_CHANNEL_0,
        waveshare_esp32_one.ledc_timer = LEDC_TIMER_0,
        waveshare_esp32_one.frame_size = FRAMESIZE_96X96,
        waveshare_esp32_one.pixel_format = PIXFORMAT_JPEG,
        waveshare_esp32_one.grab_mode = CAMERA_GRAB_WHEN_EMPTY,
        waveshare_esp32_one.fb_location = CAMERA_FB_IN_DRAM,
        waveshare_esp32_one.im_width = 96,
        waveshare_esp32_one.im_height = 96
    };


Zunächst müssen im Arduino-Sketch (.ino Datei) die Bibliotheken inkludiert werden

    #include "thk_EspCamDriver.h"
    #include "camera_boards.h"

Anschließend das Kamera-Objekt (nach Espressiv) erzeugen
    esp_image_t image;  // hier werden die Bilddaten gesichert
    thk_EspCamDriver camera(waveshare_esp32_one,
                            FRAMESIZE_96X96,
                            PIXFORMAT_GRAYSCALE,
                            &image);

Falls notwendig kann das Bild gespiegelt werden

    camera.set_hmirror(true);
    camera.set_vflip(true);

In der Setup()-Routine wird die Kamera initialisiert 

    camera.init();

In der Loop()-Routine das Bild eingelesen,

    camera.take_picture(&image)

und abschließend immer der Speicher freigeben

    camera.free_buffer(&image)

Auf die Pixel des Bildes wird über den Bild-Buffer zugegriffen

    image.buf
    image.width
    image.height