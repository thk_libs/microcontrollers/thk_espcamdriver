
#ifndef ESP_CAMERA_BOARDS_H
#define ESP_CAMERA_BOARDS_H

#include "esp_camera.h"
// Bitte um eigenes Board erweitern (falls nicht schon vorhanden!)
// Unterstützte Boards:
//      * Waveshare ESP32ONE
//      *
//      *
//      *

espboard_t waveshare_esp32_one{
    waveshare_esp32_one.cpin1 = 13,
    waveshare_esp32_one.cpin2 = 14,

    waveshare_esp32_one.pin_d0 = 34,
    waveshare_esp32_one.pin_d1 = 13,
    waveshare_esp32_one.pin_d2 = 14,
    waveshare_esp32_one.pin_d3 = 35,
    waveshare_esp32_one.pin_d4 = 39,
    waveshare_esp32_one.pin_d5 = 38,
    waveshare_esp32_one.pin_d6 = 37,
    waveshare_esp32_one.pin_d7 = 36,

    waveshare_esp32_one.pin_xclk = 4,
    waveshare_esp32_one.pin_pclk = 25,
    waveshare_esp32_one.pin_vsync = 5,
    waveshare_esp32_one.pin_href = 27,
    waveshare_esp32_one.pin_sscb_sda = 18,
    waveshare_esp32_one.pin_sscb_scl = 23,
    waveshare_esp32_one.pin_pwdn = -1,
    waveshare_esp32_one.pin_reset = -1,
    waveshare_esp32_one.xclk_freq_hz = 20000000,
    waveshare_esp32_one.jpeg_quality = 12,
    waveshare_esp32_one.fb_count = 1,

    waveshare_esp32_one.ledc_channel = LEDC_CHANNEL_0,
    waveshare_esp32_one.ledc_timer = LEDC_TIMER_0,
    waveshare_esp32_one.frame_size = FRAMESIZE_96X96,
    waveshare_esp32_one.pixel_format = PIXFORMAT_JPEG,
    // bisher getestete Framesize mit JPEG
    //  * FRAMESIZE_HQVGA,    // 240x176
    //  * FRAMESIZE_QVGA,     // 320x240
    //  * FRAMESIZE_SVGA,     // 800x600
    //  * FRAMESIZE_240X240    // 240x240
    waveshare_esp32_one.grab_mode = CAMERA_GRAB_WHEN_EMPTY,
    waveshare_esp32_one.fb_location = CAMERA_FB_IN_DRAM,
    waveshare_esp32_one.im_width = 96,
    waveshare_esp32_one.im_height = 96

};

espboard_t m5stackv2_psram_esp32{
    m5stackv2_psram_esp32.cpin1 = 13,
    m5stackv2_psram_esp32.cpin2 = 14,

    m5stackv2_psram_esp32.pin_d0 = 32,
    m5stackv2_psram_esp32.pin_d1 = 35,
    m5stackv2_psram_esp32.pin_d2 = 34,
    m5stackv2_psram_esp32.pin_d3 = 5,
    m5stackv2_psram_esp32.pin_d4 = 39,
    m5stackv2_psram_esp32.pin_d5 = 18,
    m5stackv2_psram_esp32.pin_d6 = 36,
    m5stackv2_psram_esp32.pin_d7 = 19,

    m5stackv2_psram_esp32.pin_xclk = 27,
    m5stackv2_psram_esp32.pin_pclk = 21,
    m5stackv2_psram_esp32.pin_vsync = 25,
    m5stackv2_psram_esp32.pin_href = 26,
    m5stackv2_psram_esp32.pin_sscb_sda = 22,
    m5stackv2_psram_esp32.pin_sscb_scl = 23,
    m5stackv2_psram_esp32.pin_pwdn = -1,
    m5stackv2_psram_esp32.pin_reset = 15,
    m5stackv2_psram_esp32.xclk_freq_hz = 20000000,
    m5stackv2_psram_esp32.jpeg_quality = 12,
    m5stackv2_psram_esp32.fb_count = 1,

    m5stackv2_psram_esp32.ledc_channel = LEDC_CHANNEL_0,
    m5stackv2_psram_esp32.ledc_timer = LEDC_TIMER_0,
    m5stackv2_psram_esp32.frame_size = FRAMESIZE_96X96,
    m5stackv2_psram_esp32.pixel_format = PIXFORMAT_JPEG,
    m5stackv2_psram_esp32.grab_mode = CAMERA_GRAB_WHEN_EMPTY,
    m5stackv2_psram_esp32.fb_location = CAMERA_FB_IN_DRAM,
    m5stackv2_psram_esp32.im_width = 96,
    m5stackv2_psram_esp32.im_height = 96};

#endif