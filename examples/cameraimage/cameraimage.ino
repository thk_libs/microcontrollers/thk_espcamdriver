// Beschreibung:
//  Beispiel Sketch für das Auslesen der ESP32One Waveshare Kamera
// Abhängigkeiten:
// thk_EspCamDriver.h
// Autor:
//  Vladislav Vlasuk (TH Köln, Labor für Assistenzsysteme)
// Datum:
//  20.07.2022

// Bitte ESP32 Kameramodell auskommentieren
#include <thk_EspCamDriver.h> // ESP32 Kamera
#include "camera_boards.h"

// ESP32 Kameraspezifisch
esp_image_t image;

// Objekte erzeugen
thk_EspCamDriver camera(m5stackv2_psram_esp32, // <- hier das verwendete Kameramodell auswählen (muss in camera_boards.h vorhanden sein!)
                        FRAMESIZE_96X96,
                        PIXFORMAT_GRAYSCALE,
                        &image);

void setup()
{
    Serial.begin(115200);
    // Bild spiegeln
    camera.set_hmirror(1);
    camera.set_vflip(1);
    // initialisieren
    camera.init();
};

void loop()
{
    // Bild auslesen
    camera.take_picture(&image);
    Serial.printf("Bildformat vor Skalierung: %ix%i \n", image.width, image.height);

    // Bild skalieren und ausgeben
    byte factor = 4;
    esp_image_t img_scaled;
    camera.rescale_img(&image, &img_scaled, factor, true);
    Serial.printf("Bildformat nach Skalierung(1/%i): %ix% i \n", factor, img_scaled.width, img_scaled.height);

    // Datenformat ändern (bspw. für tensorflow lite)
    Serial.println("Konvertiere Datentyp zu float\n");
    float *floatbuf = camera.convertbuffer_FLOAT32(img_scaled.buf, img_scaled.width, img_scaled.height, false);

    // Serial.println("Biarisiere Bild\n");
    // camera.binarize_FLOAT32(floatbuf, img_scaled.width * img_scaled.height, 100.0, 255.0, 0.0, true);

    // Speicher freigeben
    delete floatbuf;
    camera.free_buffer(&image);
    Serial.println("\n\n");

    // Bildschirmausgabe
    delay(1);
}
