// Beschreibung:
//  Beispiel Sketch für das Auslesen der ESP32One Waveshare Kamera
//  und lokales Streamen des Kamerabildes per HTTP
// Abhängigkeiten:
// thk_EspCamDriver.h, thk_EspCamServer.h
// Autor:
//  Vladislav Vlasuk (TH Köln, Labor für Assistenzsysteme)
// Datum:
//  05.07.2022

// Bitte ESP32 Kameramodell auskommentieren
#include <thk_EspCamDriver.h> // ESP32 Kamera
#include "camera_boards.h"
#include <thk_EspCamServer.h> // WIFI und HTTP-Server

// Wlan-Netzwerk und Internetressource (URI) festlegen
const char *ssid = "iPhone von Vladislav";
const char *password = "qqqqqqqq";
const char *uri = "/stream";

// ESP32 Kameraspezifisch
esp_image_t image;
thk_EspCamDriver camera(waveshare_esp32_one,
                        FRAMESIZE_240X240,
                        PIXFORMAT_JPEG,
                        &image);
                        
thk_EspCamServer server(ssid, password, uri);

void setup()
{
  Serial.begin(115200);
  // Bild spiegeln
  camera.set_hmirror(1);
  camera.set_vflip(1);
  // initialisieren
  camera.init(); // Kamera starten

  server.connect();                            // Wifi verbinden
  server.start_webserver(&get_stream_handler); // Server starten
};

void loop()
{
  // Do nothing. Everything is done in another task by the web server
  delay(1e4);
}

// Handler für das Auslesen des Kameramoduls am ESP32One Waveshare
esp_err_t get_stream_handler(httpd_req_t *req)
{

  Serial.println("Starte Kamera Handler");
  esp_err_t res = ESP_OK;
  char *part_buf[128];
  res = httpd_resp_set_type(req, _STREAM_CONTENT_TYPE);
  if (res != ESP_OK)
    return res;

  while (true)
  {
    // Schieße Bild
    camera.take_picture(&image);

    // This API will send the data as an HTTP response to the request in the form of chunks with chunked-encoding
    if (res == ESP_OK)
    {
      res = httpd_resp_send_chunk(req, _STREAM_BOUNDARY, strlen(_STREAM_BOUNDARY));
    }
    if (res == ESP_OK)
    {
      size_t hlen = snprintf((char *)part_buf, 128, _STREAM_PART, image.buf_len);
      res = httpd_resp_send_chunk(req, (const char *)part_buf, hlen);
    }
    if (res == ESP_OK)
    {
      res = httpd_resp_send_chunk(req, (const char *)image.buf, image.buf_len);
    }

    // close stream
    if (image.framebuffer)
    {
      camera.free_buffer(&image);
      image.buf = NULL;
    }

    if (res != ESP_OK)
    {
      Serial.println("send frame failed failed");
      break;
    }
  }
  return res;
}
