
// // Unittests des Waveshare Esp32 One.
#include <thk_EspCamDriver.h> // ESP32 Kamera
#include "camera_boards.h"
#include <AUnit.h>

thk_EspCamDriver *p_on_driver;
esp_image_t *p_on_image;
// Prüfe, ob die Referenzen im Konstruktor übernommen werden
test(t1_createobj)
{ // --- pre
    p_on_image = new esp_image_t;
    p_on_driver = new thk_EspCamDriver(waveshare_esp32_one, p_on_image);
    assertNotEqual(p_on_driver->config, NULL);
    assertNotEqual(p_on_driver->error, NULL);
    // --- post
    delete p_on_driver;
    delete p_on_image;
    p_on_driver = NULL;
    p_on_image = NULL;
}
test(t2_createobj_w_board)
{
    p_on_image = new esp_image_t;
    p_on_driver = new thk_EspCamDriver(waveshare_esp32_one,
                                       FRAMESIZE_96X96,
                                       PIXFORMAT_GRAYSCALE,
                                       p_on_image);
    // --- pre
    assertNotEqual(p_on_driver->config, NULL);
    assertNotEqual(p_on_driver->error, NULL);
    assertEqual(p_on_image->width, size_t(waveshare_esp32_one.im_width));
    assertEqual(p_on_image->height, size_t(waveshare_esp32_one.im_height));
    assertEqual(p_on_driver->board.frame_size, FRAMESIZE_96X96);
    assertEqual(p_on_driver->board.pixel_format, PIXFORMAT_GRAYSCALE);
    // --- post
    delete p_on_driver;
    delete p_on_image;
    p_on_driver = NULL;
    p_on_image = NULL;
}
// Prüfe, ob die Kamera Pins 1 und 2 übernommen werden
test(t3_setcpins)
{
    p_on_image = new esp_image_t;
    p_on_driver = new thk_EspCamDriver(waveshare_esp32_one, p_on_image);
    // --- pre
    p_on_driver->setCameraPins(1, 2);
    assertEqual(p_on_driver->board.cpin1, 1);
    assertEqual(p_on_driver->board.cpin2, 2);
    // --- post
    delete p_on_driver;
    delete p_on_image;
    p_on_driver = NULL;
    p_on_image = NULL;
}

// Prüfe, ob das config-Objekt durch die Methode mit den Pins aus camera_pins.h überschrieben wird
test(t4_createconfig)
{
    p_on_image = new esp_image_t;
    p_on_driver = new thk_EspCamDriver(waveshare_esp32_one, p_on_image);
    // --- pre
    p_on_driver->create_config();
    assertEqual(p_on_driver->config->pin_d0, waveshare_esp32_one.pin_d0);
    assertEqual(p_on_driver->config->pin_d1, waveshare_esp32_one.pin_d1);
    assertEqual(p_on_driver->config->pin_d2, waveshare_esp32_one.pin_d2);
    assertEqual(p_on_driver->config->pin_d3, waveshare_esp32_one.pin_d3);
    assertEqual(p_on_driver->config->pin_d4, waveshare_esp32_one.pin_d4);
    assertEqual(p_on_driver->config->pin_d5, waveshare_esp32_one.pin_d5);
    assertEqual(p_on_driver->config->pin_d6, waveshare_esp32_one.pin_d6);
    assertEqual(p_on_driver->config->pin_d7, waveshare_esp32_one.pin_d7);
    assertEqual(p_on_driver->config->pin_xclk, waveshare_esp32_one.pin_xclk);
    assertEqual(p_on_driver->config->pin_pclk, waveshare_esp32_one.pin_pclk);
    assertEqual(p_on_driver->config->pin_vsync, waveshare_esp32_one.pin_vsync);
    assertEqual(p_on_driver->config->pin_href, waveshare_esp32_one.pin_href);
    assertEqual(p_on_driver->config->pin_sscb_sda, waveshare_esp32_one.pin_sscb_sda);
    assertEqual(p_on_driver->config->pin_sscb_scl, waveshare_esp32_one.pin_sscb_scl);
    assertEqual(p_on_driver->config->pin_pwdn, waveshare_esp32_one.pin_pwdn);
    assertEqual(p_on_driver->config->pin_reset, waveshare_esp32_one.pin_reset);
    assertEqual(p_on_driver->config->xclk_freq_hz, waveshare_esp32_one.xclk_freq_hz);
    assertEqual(p_on_driver->config->jpeg_quality, waveshare_esp32_one.jpeg_quality);
    // ...
    // --- post
    delete p_on_driver;
    delete p_on_image;
    p_on_driver = NULL;
    p_on_image = NULL;
}

// Framebuffer sollte am Ende jedes Snapshots geleert werden, um Speicher freizugeben
test(t5_closestream_empty_framebuffer)
{
    p_on_image = new esp_image_t;
    p_on_driver = new thk_EspCamDriver(waveshare_esp32_one, p_on_image);
    // --- pre
    uint8_t arr[]={1,2,3,4,5,6,7,8,9,10};
    p_on_image->buf=arr;
    assertEqual((p_on_image->buf != NULL), true);
    p_on_driver->free_buffer(p_on_image);
    assertEqual((p_on_image->buf == NULL), true);
    // --- post
    delete p_on_driver;
    delete p_on_image;
    p_on_driver = NULL;
    p_on_image = NULL;
}

// Gerne um weitere Tests ergänzen ....

//----------------------------------------------------------------------------
void setup()
{
    delay(1000);
    Serial.begin(115200);
    while (!Serial)
        ; // for the Arduino Leonardo/Micro only
}

void loop()
{
    aunit::TestRunner::run();
}
