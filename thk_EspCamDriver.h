
#ifndef THK_ESPCAMDRIVER_H
#define THK_ESPCAMDRIVER_H

#include <Arduino.h>
#include <esp_camera.h>

typedef struct espboard espboard_t;
struct espboard
{
  int8_t cpin1;
  int8_t cpin2;
  int8_t pin_d0;
  int8_t pin_d1;
  int8_t pin_d2;
  int8_t pin_d3;
  int8_t pin_d4;
  int8_t pin_d5;
  int8_t pin_d6;
  int8_t pin_d7;

  int8_t pin_xclk;
  int8_t pin_pclk;
  int8_t pin_vsync;
  int8_t pin_href;
  int8_t pin_sscb_sda;
  int8_t pin_sscb_scl;
  int8_t pin_pwdn;
  int8_t pin_reset;
  int32_t xclk_freq_hz;
  int8_t jpeg_quality;
  int8_t fb_count;

  ledc_channel_t ledc_channel;
  ledc_timer_t ledc_timer;
  framesize_t frame_size;
  pixformat_t pixel_format; // for streaming

  int16_t im_width;
  int16_t im_height;
};

struct esp_image_t
{
  size_t width;
  size_t height;
  size_t buf_len = 0;
  uint8_t *buf = NULL; // Buffer Kamerabild
  camera_fb_t *framebuffer = NULL;
};
// Handhabung der ESP32 One Camera (Konfigurieren, Bild auslesen)
class thk_EspCamDriver
{

private:
  // Zwecks UnitTests von private als public deklarierte Variablen
public:
  espboard_t board; // Pinbelegung des Kameramoduls (Stark abhängig vom ESP32 Board!)

  camera_config_t *config = NULL;
  esp_err_t *error = NULL;
  sensor_t *camerasensor = NULL;
  bool vflip = 0;
  bool hmirror = 0;
  bool grscl = 0;

  esp_image_t *image;
  // ---

  // In Zukunft besser das Kameramodell direkt übergeben!
  thk_EspCamDriver(espboard_t board, esp_image_t *img);
  thk_EspCamDriver(espboard_t board, framesize_t s, pixformat_t f, esp_image_t *img);

  // Erzeugt eine Konfiguration basierend auf Klassenattributen
  // Muss vor dem Auslesen der Kamera erfolgen!
  void create_config();

  // Initialisiert die ESP Kamera mit der Konfiguration
  void init_camera();

  // Erzeugt ein Bild mithilfe des Kameramoduls vom Typ Zeiger auf camera_fb_t
  bool take_picture(esp_image_t *img);

  // Beendet Datenübertragung und leert allokierten Speicher
  // Muss immer vor dem nächsten take_picture() erfolgen!
  void free_buffer(esp_image_t *img);

  // Initialisiert das Kameramodul (erstellt Konfiguration, startet Kamera)
  void init();

  // Hier könnten später Funktionen zur Bildmanipulation folgen ....
  void set_vflip(bool f);

  void set_hmirror(bool f);

  void set_grayscale(bool f);

  void setCameraPins(int8_t camera_pin1, int8_t camera_pin2);

  int8_t getcpin1();
  int8_t getcpin2();

  void set_framesize(framesize_t size);
  void set_pixelformat(pixformat_t format);

  // Skaliert ein Eingangsbild image vom Typ Zeiger auf esp_image_t um factor und schreibt das neue Bild img_out vom Typ Zeiger auf esp_image_t.
  // Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
  void rescale_img(esp_image_t *image, esp_image_t *img_out, byte factor, bool serialout);

  // Konvertiere Bildbuffer von uint8_t zu int8_t
  // Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
  int8_t *convertbuffer_INT8(uint8_t *buf_uint8, int16_t width, int16_t height, bool serialout);

  // Konvertiere Bildbuffer von uint8_t zu int8_t
  // Serialout ermöglicht die serielle Ausgabe des Bildes im seriellen Monitor (ArduinoIDE empfohlen)
  float *convertbuffer_FLOAT32(uint8_t *buf_uint8, int16_t width, int16_t height, bool serialout);

  // Binarisiere Bildbuffer der Länge len. Hierbei wird jeweils ein Pixel min oder max (>Schwellwert) gesetzt.
  // In Zukunft besser als Template implementieren!
  void binarize_FLOAT32(float *buf_float, int32_t len, int16_t threshold, float max, float min, bool serialout);
};

#endif